#include "SerialCom.h"
#include "Logger.h"


#include <fcntl.h>
#include <unistd.h>
#include <termios.h>      

#include <cerrno>
#include <cstring>
#include <stdexcept>

loglevel_e loglevel;

SerialCom::SerialCom(const std::string& deviceName, speed_t baud, bool parityBit, bool twoStopBits, bool flowControl, CharSize charsize)
: m_deviceName(deviceName), m_baudrate(baud), m_parityBit(parityBit), m_twoStopBits(twoStopBits), m_flowControl(flowControl), m_charsize(charsize)
{
  this->init(); 
}

SerialCom::~SerialCom()
{
  if(m_good) close(m_dev);
}

void SerialCom::init() {
  m_dev = open(m_deviceName.c_str(), O_RDWR | O_NOCTTY);
  this->config();
  m_good = true;
}

void SerialCom::config()
{
  logger(logDEBUG3) << __PRETTY_FUNCTION__;

  logger(logDEBUG3) << "Configuring serial device " << m_deviceName;
  logger(logDEBUG3) << "  Baud Rate: " << m_baudrate;
  logger(logDEBUG3) << "  Enable parity bit: " << m_parityBit;
  logger(logDEBUG3) << "  Use two stop bits: " << m_twoStopBits;
  logger(logDEBUG3) << "  Enable hardware flow control: " << m_flowControl;
  logger(logDEBUG3) << "  Character size: " << m_charsize;
  
  //Asynchronous read
  fcntl(m_dev, F_SETFL, O_NONBLOCK);
  
  if (tcgetattr(m_dev, &m_tty))
    {
      logger(logERROR) << __PRETTY_FUNCTION__ << " -> Could not get tty attributes!";
      m_good = false;
    }

  m_tty_old = m_tty;

  cfsetospeed(&m_tty, m_baudrate);
  cfsetispeed(&m_tty, m_baudrate);

  if(m_parityBit)
    m_tty.c_cflag &=  PARENB;
  else
    m_tty.c_cflag &= ~PARENB;

  if(m_twoStopBits)
    m_tty.c_cflag &=  CSTOPB;
  else
    m_tty.c_cflag &= ~CSTOPB;

  if(m_flowControl)
    m_tty.c_cflag &=  CRTSCTS;
  else
    m_tty.c_cflag &= ~CRTSCTS;

  m_tty.c_cflag &= ~CSIZE;
  m_tty.c_cflag |= m_charsize;
  m_tty.c_cflag |= CREAD | CLOCAL;	// turn on READ & ignore ctrl lines

  cfmakeraw(&m_tty);
  m_tty.c_lflag &= ~(ICANON|ECHO);	// Non-canonical mode, no echo

  tcflush(m_dev, TCIFLUSH);		// Empty buffers

  if (tcsetattr(m_dev, TCSANOW, &m_tty))
    {
      logger(logERROR) << __PRETTY_FUNCTION__ << " -> Could not set tty attributes!";
      m_good = false;
    }
}

int SerialCom::flush()
{
  return tcflush(m_dev, TCIFLUSH);
}

int SerialCom::write(char *buf, size_t length)
{
  int n_write = ::write(m_dev, buf, length);
  if(n_write<0)
    {
      logger(logERROR) << std::strerror(errno);
      throw std::runtime_error("Error writing to "+m_deviceName+": "+std::strerror(errno));
    }
  
  return n_write;
}

int SerialCom::read(char *buf, size_t length)
{
  int n_read = ::read(m_dev, buf, length);
  if(n_read<0)
    {
      logger(logERROR) << std::strerror(errno);
      throw std::runtime_error("Error reading from "+m_deviceName+": "+std::strerror(errno));
    }

  return n_read;
}

int SerialCom::write(const std::string& buf)
{
  int n_write = ::write(m_dev, buf.c_str(), buf.size());
  if(n_write<0)
    {
      logger(logERROR) << std::strerror(errno);
      throw std::runtime_error("Error writing to "+m_deviceName+": "+std::strerror(errno));
    }

  return n_write;
}

int SerialCom::read(std::string &buf) 
{
  int n_read = ::read(m_dev, m_tmpbuf, MAX_READ);
  if(n_read>=0)
    {
      buf=std::string(m_tmpbuf, n_read);
    }
  else
    {
      throw std::runtime_error("Error reading from "+m_deviceName+": "+std::strerror(errno));
      logger(logERROR) << std::strerror(errno);
    }

  return n_read;
}

bool SerialCom::is_open()
{
  return m_good;
}

