# Examples 

## Power supply: control_powersupply_example
To run it, use:
./build/bin/control_powersupply_example ./src/libEquipConf/input-hw.json low-voltage

This simple script shows how to create the hardware database and how to configure one of the channels listed (low-voltage). Then it programs the channel using the settings in the program block of the json file (this is optional), it powers the channel on, returns the measured voltage and current and powers it off.

To access functions that are only defined for a specific power supply, one needs to cast the pointer to the IPowerSupply object back to
the specific class. For example, beefOff() is a function that is implemented only for AgilentPS.  

The advantage of using the logical channel (PowerSupplyChannel) is that the same executable will work regardless of the power supply being 
multi- or single-channel. The only change must be in the json file.

