#include <iostream>
#include <memory>
#include <iomanip>
#include <unistd.h>
#include <math.h>

#include "Logger.h"
#include "I2CFTDICom.h"
#include "SHT85.h"

loglevel_e loglevel = logINFO;

int main(int argc, char*argv[])
{
#ifdef FTDI
  std::shared_ptr<I2CCom> i2c=std::make_shared<I2CFTDICom>(0x44);
  std::shared_ptr<ClimateSensor> sensor=std::make_shared<SHT85>(i2c);

  for(uint32_t i=0;i<10000;i++)
    {
      sensor->read();
      std::cout << sensor->temperature() << "\t" << sensor->humidity() << std::endl;
    }
#else
  std::cerr << "Need FTDI support!" << std::endl;
  return 1;
#endif

  return 0;
}

