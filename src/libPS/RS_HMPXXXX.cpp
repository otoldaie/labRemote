#include "RS_HMPXXXX.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(RS_HMPXXXX)

RS_HMPXXXX::RS_HMPXXXX(const std::string& name) :
IPowerSupply(name)
{ }

void RS_HMPXXXX::connect()
{
  if (m_config["communication"]["protocol"] == "usb")
    {
      connect_usb(m_config["communication"]["port"]);

      std::string result = this->receive("*IDN?");
      if (result.empty())
	{
	  logger(logERROR) << "Failed communication with the PS";
	  throw std::runtime_error("Failed communication with the PS");
	}
    }
  else
    {
      logger(logERROR) << "Unknown communication protocol for hardware " << m_name << ": " << m_config["communication"]["protocol"];
      throw std::runtime_error("Unknown communication protocol for hardware " + m_name + ": " + m_config["communication"]["protocol"].get<std::string>());
    }
}

void RS_HMPXXXX::connect_usb(const std::string& dev)
{
  m_com.reset(new SerialCom(dev));
}

void RS_HMPXXXX::checkCompatibilityList()
{
  // Check if the PS belong to the list of the support PS models
  std::string idn=this->receive("*IDN?");

  if(idn.find("ROHDE&SCHWARZ,HMP4040") == std::string::npos)
    {
      logger(logERROR)<< "Unknown power supply: " << idn;
      throw std::runtime_error("Unknown power supply: " + idn);
    }
}

void RS_HMPXXXX::reset()
{
  send("*RST");

  std::string result=receive("*IDN?");
  if(result.empty())
    throw std::runtime_error("No communication after reset.");
}

void RS_HMPXXXX::turnOn(unsigned channel)
{
  send("INST:NSEL " + std::to_string(channel));
  send("OUTPUT ON");
}

void RS_HMPXXXX::turnOff(unsigned channel)
{
  send("INST:NSEL " + std::to_string(channel));
  send("OUTPUT OFF");
}

void RS_HMPXXXX::setCurrentLevel(double curr, unsigned channel)
{
  setCurrentProtect(curr, channel);
}

double RS_HMPXXXX::getCurrentLevel(unsigned channel)
{
  return std::stod(receive("INST:NSEL " + std::to_string(channel) + "\n" + "CURR?"));
}

void RS_HMPXXXX::setCurrentProtect(double maxcur, unsigned channel)
{
  send("INST:NSEL " + std::to_string(channel));
  send("CURR "+ std::to_string(maxcur));
}

double RS_HMPXXXX::getCurrentProtect(unsigned channel)
{
  return getCurrentLevel(channel);
}

double RS_HMPXXXX::measureCurrent(unsigned channel)
{
  return std::stod(receive("INST:NSEL " + std::to_string(channel) + "\n" + "MEAS:CURR?"));
}

void RS_HMPXXXX::setVoltageLevel(double volt, unsigned channel)
{
  send("INST:NSEL " + std::to_string(channel));
  send("VOLT "+ std::to_string(volt));
}

double RS_HMPXXXX::getVoltageLevel(unsigned channel)
{
  return std::stod(receive("INST:NSEL " + std::to_string(channel) + "\n" + "VOLT?"));
}

void RS_HMPXXXX::setVoltageProtect(double maxvolt, unsigned channel)
{
  send("INST:NSEL " + std::to_string(channel));
  send("VOLT:PROT " + std::to_string(maxvolt));
}

double RS_HMPXXXX::getVoltageProtect(unsigned channel)
{
  return getVoltageLevel(channel);
}

double RS_HMPXXXX::measureVoltage(unsigned channel)
{
  return std::stod(receive("INST:NSEL " + std::to_string(channel) + "\n" + "MEAS:VOLT?"));
}

void RS_HMPXXXX::send(const std::string& cmd)
{
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
  m_com->write(cmd + "\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
}

std::string RS_HMPXXXX::receive(const std::string& cmd)
{
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
  m_com->write(cmd + "\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
  std::string buf;
  m_com->read(buf);
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Received: " << buf;
  return buf;
}
