#include "TTIMX180TPPs.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(TTIMX180TPPs)

TTIMX180TPPs::TTIMX180TPPs(const std::string& name) :
IPowerSupply(name)
{ }

void TTIMX180TPPs::connect()
{
  if (m_config["communication"]["protocol"] == "usb")
    {
      connect_usb(m_config["communication"]["port"]);

      std::string result=this->receive("*IDN?");
      if (result.empty())
	{
	  std::cerr << "Failed communication with the PS "<<result<<std::endl;
	  throw std::runtime_error("Failed communication with the PS");
	}
    }
  else
    {
      logger(logERROR) << "Unknown communication protocol for hardware " << m_name << ": " << m_config["communication"]["protocol"];
      throw std::runtime_error("Unknown communication protocol for hardware " + m_name + ": " + m_config["communication"]["protocol"].get<std::string>());
    }
}

void TTIMX180TPPs::connect_usb(const std::string& dev)
{
  m_com.reset(new SerialCom(dev));
  m_com->write("++auto 0\n\r");
}

void TTIMX180TPPs::checkCompatibilityList()
{

  // Check if the PS belong to the list of the support PS models
  std::string idn=this->receive("*IDN?");

  //Add here other version of support Keithley 24xx power supplies
  if(idn.find("THURLBY THANDAR, MX180TP") == std::string::npos)
    {
      logger(logERROR)<< "Unknown power supply: " << idn;
      throw std::runtime_error("Unknown power supply: " + idn);
    }
}

void TTIMX180TPPs::reset()
{
  send("OPALL 0");
  send("*RST");

  std::string result=receive("*IDN?");
  if(result.empty())
    throw std::runtime_error("No communication after reset.");
}

void TTIMX180TPPs::turnOn(unsigned channel)
{
  if(channel!=1 && channel!=2 && channel!=3)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  send("OP"+std::to_string(channel)+" 1");
}

void TTIMX180TPPs::turnOff(unsigned channel)
{
  if(channel!=1 && channel!=2 && channel!=3)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  send("OP"+std::to_string(channel)+" 0");
}

void TTIMX180TPPs::setCurrentLevel(double cur, unsigned channel)
{
  if(channel!=1 && channel!=2 && channel!=3)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  send("I" + std::to_string(channel)+" "+std::to_string(cur));
}

double TTIMX180TPPs::getCurrentLevel(unsigned channel)
{
  if(channel!=1 && channel!=2 && channel!=3)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  return std::stod(receive("I" + std::to_string(channel) + "?").substr(0,5));
}

void TTIMX180TPPs::setCurrentProtect(double maxcur, unsigned channel)
{
  setCurrentLevel(maxcur, channel);
}

double TTIMX180TPPs::getCurrentProtect(unsigned channel)
{
  return getCurrentLevel(channel);
}

double TTIMX180TPPs::measureCurrent(unsigned channel)
{
  if(channel!=1 && channel!=2 && channel!=3)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  return std::stod(receive("I" + std::to_string(channel) + "O?").substr(0,5));
}

void TTIMX180TPPs::setVoltageLevel(double volt, unsigned channel)
{
  if(channel!=1 && channel!=2 && channel!=3)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  send("V" + std::to_string(channel)+" "+std::to_string(volt));
}

double TTIMX180TPPs::getVoltageLevel(unsigned channel)
{
  if(channel!=1 && channel!=2 && channel!=3)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  return std::stod(receive("V" + std::to_string(channel) + "?").substr(0,5));
}

void TTIMX180TPPs::setVoltageProtect(double maxvolt, unsigned channel)
{
  setVoltageLevel(maxvolt, channel);
}

double TTIMX180TPPs::getVoltageProtect(unsigned channel)
{
  return getVoltageLevel(channel);
}

double TTIMX180TPPs::measureVoltage(unsigned channel)
{
  if(channel!=1 && channel!=2 && channel!=3)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  return std::stod(receive("V" + std::to_string(channel) + "O?").substr(0,5));
}

void TTIMX180TPPs::send(const std::string& cmd)
{
  m_com->write("++addr " + std::to_string(m_config["communication"]["gpib_addr"].get<uint32_t>()) + "\n\r");
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
  m_com->write(cmd+"\n\r");
  std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
}

std::string TTIMX180TPPs::receive(const std::string& cmd)
{
  m_com->write("++addr " + std::to_string(m_config["communication"]["gpib_addr"].get<uint32_t>()) + "\n\r");
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
  m_com->write(cmd+"\n\r");
  m_com->write("++read eoi\n\r");
  std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
  std::string buf;
  m_com->read(buf);
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Received: " << buf;
  return buf;
}
