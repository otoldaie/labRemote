#include "Keithley22XX.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(Keithley22XX)

Keithley22XX::Keithley22XX(const std::string& name) :
IPowerSupply(name)
{ }

void Keithley22XX::connect()
{
  if (m_config["communication"]["protocol"] == "usb")
    {
      connect_usb(m_config["communication"]["port"]);

      std::string result = this->receive("*IDN?");
      if (result.empty())
	{
	  logger(logERROR) << "Failed communication with the PS";
	  throw std::runtime_error("Failed communication with the PS");
	}
    }
  else
    {
      logger(logERROR) << "Unknown communication protocol for hardware " << m_name << ": " << m_config["communication"]["protocol"];
      throw std::runtime_error("Unknown communication protocol for hardware " + m_name + ": " + m_config["communication"]["protocol"].get<std::string>());
    }
}

void Keithley22XX::connect_usb(const std::string& dev)
{
  m_com.reset(new SerialCom(dev));
  m_com->write("++auto 0\n\r");
}

void Keithley22XX::checkCompatibilityList()
{
  // Check if the PS belong to the list of the support PS models
  std::string idn=this->receive("*IDN?");

  //Add here other version of support Keithley 22xx power supplies
  if(idn.find("Keithley instruments, 2231A-30-3") == std::string::npos)
    {
      logger(logERROR)<< "Unknown power supply: " << idn;
      throw std::runtime_error("Unknown power supply: " + idn);
    }
}

void Keithley22XX::reset()
{
  send("OUTP:ALL 0");
  send("*RST");

  std::string result=receive("*IDN?");
  if(result.empty())
    throw std::runtime_error("No communication after reset.");
}

void Keithley22XX::turnOn(unsigned channel)
{
  if(channel!=1 && channel!=2 && channel!=3)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  send("INST CH" + std::to_string(channel));
  send("CHAN:OUTP 1");
}

void Keithley22XX::turnOff(unsigned channel)
{
  if(channel!=1 && channel!=2 && channel!=3)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  send("INST CH" + std::to_string(channel));
  send("CHAN:OUTP 0");
}

void Keithley22XX::setCurrentLevel(double cur, unsigned channel)
{
  if(channel!=1 && channel!=2 && channel!=3)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  send("INST CH" + std::to_string(channel));
  send(":SOURCE:FUNC CURR");
  send(":SOURCE:CURR " + std::to_string(cur));
}

double Keithley22XX::getCurrentLevel(unsigned channel)
{
  if(channel!=1 && channel!=2 && channel!=3)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  send("INST CH" + std::to_string(channel));
  return std::stod(receive(":SOURCE:CURR?"));
}

void Keithley22XX::setCurrentProtect(double maxcur, unsigned channel)
{
  if(channel!=1 && channel!=2 && channel!=3)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  send("INST CH" + std::to_string(channel));
  send(":SENSE:CURR:PROTECTION " + std::to_string(maxcur));
}

double Keithley22XX::getCurrentProtect(unsigned channel)
{
  if(channel!=1 && channel!=2 && channel!=3)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  send("INST CH" + std::to_string(channel));
  return std::stod(receive(":SENSE:CURR:PROTECTION?"));
}

double Keithley22XX::measureCurrent(unsigned channel)
{
  if(channel!=1 && channel!=2 && channel!=3)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  send(":FORMAT:ELEMENTS CURR");
  send("INST CH" + std::to_string(channel));
  return std::stod(receive(":MEAS:CURR?"));
}

void Keithley22XX::setVoltageLevel(double volt, unsigned channel)
{
  if(channel!=1 && channel!=2 && channel!=3)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  send("INST CH" + std::to_string(channel));
  send(":SOURCE:FUNC VOLT");
  send(":SOURCE:VOLT " + std::to_string(volt));
}

double Keithley22XX::getVoltageLevel(unsigned channel)
{
  if(channel!=1 && channel!=2 && channel!=3)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  send("INST CH" + std::to_string(channel));
  return std::stod(receive(":SOURCE:VOLT?"));
}

void Keithley22XX::setVoltageProtect(double maxvolt, unsigned channel)
{
  if(channel!=1 && channel!=2 && channel!=3)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  send("INST CH" + std::to_string(channel));
  send(":SENSE:VOLT:PROTECTION " + std::to_string(maxvolt));
}

double Keithley22XX::getVoltageProtect(unsigned channel)
{
  if(channel!=1 && channel!=2 && channel!=3)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  send("INST CH" + std::to_string(channel));
  return std::stod(receive(":SENSE:VOLT:PROTECTION?"));
}

double Keithley22XX::measureVoltage(unsigned channel)
{
  if(channel!=1 && channel!=2 && channel!=3)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  send(":FORMAT:ELEMENTS VOLT");
  send("INST CH" + std::to_string(channel));
  return std::stod(receive(":MEAS:VOLT?"));
}

void Keithley22XX::send(const std::string& cmd)
{
  m_com->write("++addr " + std::to_string(m_config["communication"]["gpib_addr"].get<uint32_t>()) + "\n\r");
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
  m_com->write(cmd+"\n\r");
  std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
}

std::string Keithley22XX::receive(const std::string& cmd)
{
  m_com->write("++addr " + std::to_string(m_config["communication"]["gpib_addr"].get<uint32_t>()) + "\n\r");
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
  m_com->write(cmd+"\n\r");
  m_com->write("++read eoi\n\r");
  std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
  std::string buf;
  m_com->read(buf);
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Received: " << buf;
  return buf;
}
