#include "Bk16XXPs.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(Bk16XXPs)


Bk16XXPs::Bk16XXPs(const std::string& name) :
IPowerSupply(name)
{ }

void Bk16XXPs::connect()
{
  if (m_config["communication"]["protocol"] == "usb")
    connect_usb(m_config["communication"]["port"],B9600);
  else
    {
      logger(logERROR) << "Unknown communication protocol for hardware " << m_name << ": " << m_config["communication"]["protocol"];
      throw std::runtime_error("Unknown communication protocol for hardware " + m_name + ": " + m_config["communication"]["protocol"].get<std::string>());
    }
  
  if (receive("GETD").empty()){
    logger(logERROR) << "Failed communication with the PS";
    throw std::runtime_error("Failed communication with the PS");
  }
}

void Bk16XXPs::connect_usb(const std::string& dev,const speed_t& baud )
{
  m_com.reset(new SerialCom(dev, baud));
}

void Bk16XXPs::checkCompatibilityList()
{
  logger(logWARNING)<<"No checkCompatibilityList function can be implemented for Bk16kk PS. This implementation is valid only for 1687B and 1688B ";
}

void Bk16XXPs::reset()
{
  logger(logINFO)<<"No reset function can be implemented for Bk16kk PS ";
}

void Bk16XXPs::turnOn(unsigned channel )
{

  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");
  send("SOUT0");
}

void Bk16XXPs::turnOff(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");
  send("SOUT1");
}

void Bk16XXPs::setCurrentLevel(double curr, unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  curr *= 10;
  int curr_int = (int)curr;
  if (curr_int < 100)
    send("CURR0" + std::to_string(curr_int));
  else
    send("CURR" + std::to_string(curr_int));
}

double Bk16XXPs::getCurrentLevel(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  std::string volt_curr = receive("GETS");
  volt_curr = volt_curr.substr(0, volt_curr.find("\r"));
  int volt_curr_int = stoi(volt_curr);
  int curr = volt_curr_int % 1000;
  return (double)curr / 10.0;
}



void Bk16XXPs::setCurrentProtect(double curr, unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  curr *= 10;
  int curr_int = (int)curr;
  if (curr_int < 100)
    send("SOCP0" + std::to_string(curr_int));
  else
    send("SOCP" + std::to_string(curr_int));
}

double Bk16XXPs::getCurrentProtect(unsigned channel)
{

  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  std::string max_curr = receive("GOCP");
  std::string current = max_curr.substr(0, max_curr.find("\r"));
  int curr_int = stoi(current);
  double curr = floor((double)curr_int / 10);
  return curr;
}

double Bk16XXPs::measureCurrent(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  std::string volt_curr = receive("GETD");
  volt_curr = volt_curr.substr(0, volt_curr.find("\r"));
  int volt_curr_int = std::stoi(volt_curr);
  double curr = floor((double)(volt_curr_int % 100000) / 10);
  return curr / 100;
}

void Bk16XXPs::setVoltageLevel(double volt,unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");
  volt *= 10;
  int volt_int = (int)volt;
  if (volt_int < 100)
    send("VOLT0" + std::to_string(volt_int));
  else
    send("VOLT" + std::to_string(volt_int));

}

double Bk16XXPs::getVoltageLevel(unsigned channel)
{

 if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  std::string volt_curr = receive("GETS");
  volt_curr = volt_curr.substr(0, volt_curr.find("\r"));
  int volt_curr_int = stoi(volt_curr);
  double volt = floor((double)volt_curr_int / 1000);
  return volt / 10;
}

void Bk16XXPs::setVoltageProtect(double volt, unsigned channel)
{
 if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  volt *= 10;
  int volt_int = (int)volt;
  if (volt_int < 100)
    send("SOVP0" + std::to_string(volt_int));
  else
    send("SOVP" + std::to_string(volt_int));
}

double Bk16XXPs::getVoltageProtect(unsigned channel)
{
 if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  std::string max_volt = receive("GOVP");
  std::string voltage = max_volt.substr(0, max_volt.find("\r"));
  int volt_int = stoi(voltage);
  double volt = floor((double)volt_int / 10);
  return volt;
}

double Bk16XXPs::measureVoltage(unsigned channel)
{
 if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  std::string volt_curr = receive("GETD");
  volt_curr = volt_curr.substr(0, volt_curr.find("\r"));
  int volt_curr_int = stoi(volt_curr);
  double volt = floor((double)volt_curr_int / 100000);
  return volt / 100;
}

void Bk16XXPs::send(const std::string& cmd)
{
  m_com->write(cmd+"\r");
  std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
  std::string buf;
  m_com->read(buf);
  logger(logDEBUG)<<"Cmd sent: "+ cmd + " PS return: " + buf.substr(0, buf.find("\r"))+ " " + buf.substr(buf.find("\r")+1);
}

std::string Bk16XXPs::receive(const std::string& cmd)
{
  m_com->write(cmd+"\r");
  std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
  std::string buf;
  m_com->read(buf);
  logger(logDEBUG)<<"Cmd sent: "+ cmd + " PS return: " + buf.substr(0, buf.find("\r"))+ " " + buf.substr(buf.find("\r")+1);
  return buf;
}






