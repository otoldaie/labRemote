#include "RigolDP832.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(RigolDP832)


RigolDP832::RigolDP832(const std::string& name) :
IPowerSupply(name)
{ }

void RigolDP832::connect()
{
  if (m_config["communication"]["protocol"] == "usb")
    {
      connect_usb(m_config["communication"]["port"]);

      std::string result = this->receive("*IDN?");
      if (result.empty())
	{
	  logger(logERROR) << "Failed communication with the PS";
	  throw std::runtime_error("Failed communication with the PS");
	}
    }
  else
    {
      logger(logERROR) << "Unknown communication protocol for hardware " << m_name << ": " << m_config["communication"]["protocol"];
      throw std::runtime_error("Unknown communication protocol for hardware " + m_name + ": " + m_config["communication"]["protocol"].get<std::string>());
    }
}

void RigolDP832::connect_usb(const std::string& dev)
{
  m_com.reset(new SerialCom(dev));
}

void RigolDP832::checkCompatibilityList()
{
  // Check if the PS belong to the list of the support PS models
  std::string idn=this->receive("*IDN?");

  //Add here other version of support Rigol powr supplies
  if(idn.find("RIGOL TECHNOLOGIES,DP832") == std::string::npos)
    {
      logger(logERROR)<< "Unknown power supply: " << idn;
      throw std::runtime_error("Unknown power supply: " + idn);
    }
}

void RigolDP832::reset()
{
  send("*RST");

  std::string result=receive("*IDN?");
  if(result.empty())
    throw std::runtime_error("No communication after reset.");
}

void RigolDP832::turnOn(unsigned channel)
{
  if(channel<1 || 3<channel)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  send(":OUTP CH" + std::to_string(channel) + ",ON");
}

void RigolDP832::turnOff(unsigned channel)
{
  if(channel<1 || 3<channel)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));
  
  send(":OUTP CH" + std::to_string(channel) + ",OFF");
}

void RigolDP832::setCurrentLevel(double cur, unsigned channel)
{
  if(channel<1 || 3<channel)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));
  
  send(":SOUR"+std::to_string(channel)+":CURR "+ std::to_string(cur));
}

double RigolDP832::getCurrentLevel(unsigned channel)
{
  if(channel<1 || 3<channel)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  return std::stod(receive(":SOUR"+std::to_string(channel)+":CURR?"));
}

void RigolDP832::setCurrentProtect(double maxcur, unsigned channel)
{
  if(channel<1 || 3<channel)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));
  
  send(":SOUR"+std::to_string(channel)+":CURR:PROT " + std::to_string(maxcur));
}

double RigolDP832::getCurrentProtect(unsigned channel)
{
  if(channel<1 || 3<channel)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  return std::stod(receive(":SOUR"+std::to_string(channel)+":CURR:PROT?"));
}

double RigolDP832::measureCurrent(unsigned channel)
{
  if(channel<1 || 3<channel)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));
    
  return std::stod(receive(":MEAS:CURR? CH" + std::to_string(channel)));
}

void RigolDP832::setVoltageLevel(double volt, unsigned channel)
{
  if(channel<1 || 3<channel)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  send(":SOUR"+std::to_string(channel)+":VOLT "+ std::to_string(volt));
}

double RigolDP832::getVoltageLevel(unsigned channel)
{
  if(channel<1 || 3<channel)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  return std::stod(receive(":SOUR"+std::to_string(channel)+":VOLT?"));  
}

void RigolDP832::setVoltageProtect(double maxvolt, unsigned channel)
{
  if(channel<1 || 3<channel)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  send(":SOUR"+std::to_string(channel)+":VOLT:PROT " + std::to_string(maxvolt));
}

double RigolDP832::getVoltageProtect(unsigned channel)
{
  if(channel<1 || 3<channel)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  return std::stod(receive(":SOUR"+std::to_string(channel)+":VOLT:PROT?"));  
}

double RigolDP832::measureVoltage(unsigned channel)
{
  if(channel<1 || 3<channel)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  return std::stod(receive(":MEAS? CH" + std::to_string(channel)));
}

void RigolDP832::send(const std::string& cmd)
{
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
  m_com->write(cmd);
  std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
}

std::string RigolDP832::receive(const std::string& cmd)
{
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
  m_com->write(cmd);
  std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
  std::string buf;
  m_com->read(buf);
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Received: " << buf;
  return buf;
}
