#include "AgilentPs.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(AgilentPs)

AgilentPs::AgilentPs(const std::string& name, unsigned maxChannels, const std::vector<std::string>& models) :
IPowerSupply(name), m_maxChannels(maxChannels), m_models(models)
{ }

void AgilentPs::connect()
{
  if (m_config["communication"]["protocol"] == "usb")
    {
      connect_usb(m_config["communication"]["port"]);

      std::string result = this->receive("*IDN?");
      if (result.empty())
	{
	  logger(logERROR) << "Failed communication with the PS";
	  throw std::runtime_error("Failed communication with the PS");
	}
    }
  else
    {
      logger(logERROR) << "Unknown communication protocol for hardware " << m_name << ": " << m_config["communication"]["protocol"];
      throw std::runtime_error("Unknown communication protocol for hardware " + m_name + ": " + m_config["communication"]["protocol"].get<std::string>());
    }
}

void AgilentPs::connect_usb(const std::string& dev)
{
  m_com.reset(new SerialCom(dev));
  m_com->write("++auto 0\n\r");
}

void AgilentPs::checkCompatibilityList()
{
  if(m_models.empty())
    {
      logger(logWARNING) << "No compatibiliy check in AgilentPs.";
      return;
    }

  // Check if the PS belong to the list of the support PS models
  std::string idn=this->receive("*IDN?");

  //Add here other version of support Keithley 234x power supplies
  for(const std::string& model : m_models)
    {
      if(idn.find(model) != std::string::npos)
	return; // Found a working PS
    }

  logger(logERROR)<< "Unknown power supply: " << idn;
  throw std::runtime_error("Unknown power supply: " + idn);
}

void AgilentPs::reset()
{
  send("OUTPUT OFF");
  send("*RST");
  send("TRIGGER:SOURCE IMM");
  send("VOLT:PROT MAX");
  send("SYST:BEEP:STAT OFF");//disable beep

  std::string result=receive("*IDN?");
  if(result.empty())
    throw std::runtime_error("No communication after reset.");
}

void AgilentPs::turnOn(unsigned channel)
{
  setChannel(channel);
  send("OUTPUT ON");
}

void AgilentPs::turnOff(unsigned channel)
{
  setChannel(channel);
  send("OUTPUT OFF");
}

void AgilentPs::setChannel(unsigned channel)
{
  if(m_maxChannels>0)
    { // In-range channel check
      if(channel>m_maxChannels)
	throw std::runtime_error("Invalid channel: "+std::to_string(channel));
    }

  if(m_maxChannels==1) return; // No need to change channel

  send("INST:NSEL " + std::to_string(channel));
}

void AgilentPs::setCurrentLevel(double cur, unsigned channel)
{
  setChannel(channel);
  send("CURRENT " + std::to_string(cur));
}

double AgilentPs::getCurrentLevel(unsigned channel)
{
  setChannel(channel);
  return std::stod(receive("CURRENT?"));
}

void AgilentPs::setCurrentProtect(double maxcur, unsigned channel)
{
  setChannel(channel);
  send("CURRENT " + std::to_string(maxcur));
}

double AgilentPs::getCurrentProtect(unsigned channel)
{
  return getCurrentLevel();
}

double AgilentPs::measureCurrent(unsigned channel)
{
  setChannel(channel);
  return std::stod(receive("MEAS:CURR?"));
}

void AgilentPs::setVoltageLevel(double volt, unsigned channel)
{
  setChannel(channel);
  send("VOLTAGE " + std::to_string(volt));
}

double AgilentPs::getVoltageLevel(unsigned channel)
{
  setChannel(channel);
  return std::stod(receive("VOLTAGE?"));
}

void AgilentPs::setVoltageProtect(double maxvolt, unsigned channel)
{
  setChannel(channel);
  send("VOLTAGE " + std::to_string(maxvolt));
}

double AgilentPs::getVoltageProtect(unsigned channel)
{
  return getVoltageLevel();
}

double AgilentPs::measureVoltage(unsigned channel)
{
  setChannel(channel);
  return std::stod(receive("MEAS:VOLT?"));
}

void AgilentPs::beepOff()
{
  send("SYST:BEEP:STAT OFF"); // disable beep
}

void AgilentPs::send(const std::string& cmd)
{
  m_com->write("++addr " + std::to_string(m_config["communication"]["gpib_addr"].get<uint32_t>()) + "\n\r");
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
  m_com->write(cmd+"\n\r");
  std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
}

std::string AgilentPs::receive(const std::string& cmd)
{
  m_com->write("++addr " + std::to_string(m_config["communication"]["gpib_addr"].get<uint32_t>()) + "\n\r");
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
  m_com->write(cmd+"\n\r");
  m_com->write("++read eoi\n\r");
  std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
  std::string buf;
  m_com->read(buf);
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Received: " << buf;
  return buf;
}
