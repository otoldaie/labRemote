#include "SorensenPs.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(SorensenPs)

SorensenPs::SorensenPs(const std::string& name) :
IPowerSupply(name)
{ }

void SorensenPs::connect()
{
  if (m_config["communication"]["protocol"] == "usb")
    {
      connect_usb(m_config["communication"]["port"]);

      std::string result=this->receive("*IDN?");
      if (result.empty())
	{
	  logger(logERROR) << "Failed communication with the PS";
	  throw std::runtime_error("Failed communication with the PS");
	}
    }
  else
    {
      logger(logERROR) << "Unknown communication protocol for hardware " << m_name << ": " << m_config["communication"]["protocol"];
      throw std::runtime_error("Unknown communication protocol for hardware " + m_name + ": " + m_config["communication"]["protocol"].get<std::string>());
    }
}

void SorensenPs::connect_usb(const std::string& dev)
{
  m_com.reset(new SerialCom(dev));
  m_com->write("++auto 0\n\r");
}

void SorensenPs::checkCompatibilityList()
{

  // Check if the PS belong to the list of the support PS models
  std::string idn=this->receive("*IDN?");

  //Add here other version of support Keithley 24xx power supplies
  if(idn.find("SORENSEN, XPF 60-20DP") == std::string::npos)
    {
      logger(logERROR)<< "Unknown power supply: " << idn;
      throw std::runtime_error("Unknown power supply: " + idn);
    }
}

void SorensenPs::reset()
{
  send("OPALL 0");
  send("*RST");

  std::string result=receive("*IDN?");
  if(result.empty())
    throw std::runtime_error("No communication after reset.");
}

void SorensenPs::turnOn(unsigned channel)
{
  if(channel!=1 && channel!=2)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  send("OP"+std::to_string(channel)+" 1");
}

void SorensenPs::turnOff(unsigned channel)
{
  if(channel!=1 && channel!=2)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  send("OP"+std::to_string(channel)+" 0");
}

void SorensenPs::setCurrentLevel(double cur, unsigned channel)
{
  if(channel!=1 && channel!=2)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  send("I" + std::to_string(channel)+" "+std::to_string(cur));
}

double SorensenPs::getCurrentLevel(unsigned channel)
{
  if(channel!=1 && channel!=2)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  return std::stod(receive("I" + std::to_string(channel) + "?").substr(3));
}

void SorensenPs::setCurrentProtect(double maxcur, unsigned channel)
{
  setCurrentLevel(maxcur, channel);
}

double SorensenPs::getCurrentProtect(unsigned channel)
{
  return getCurrentLevel(channel);
}

double SorensenPs::measureCurrent(unsigned channel)
{
  if(channel!=1 && channel!=2)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  return std::stod(receive("I" + std::to_string(channel) + "O?").substr(0,5));
}

void SorensenPs::setVoltageLevel(double volt, unsigned channel)
{
  if(channel!=1 && channel!=2)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  send("V" + std::to_string(channel)+" "+std::to_string(volt));
}

double SorensenPs::getVoltageLevel(unsigned channel)
{
  if(channel!=1 && channel!=2)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  return std::stod(receive("V" + std::to_string(channel) + "?").substr(3));
}

void SorensenPs::setVoltageProtect(double maxvolt, unsigned channel)
{
  setVoltageLevel(maxvolt, channel);
}

double SorensenPs::getVoltageProtect(unsigned channel)
{
  return getVoltageLevel(channel);
}

double SorensenPs::measureVoltage(unsigned channel)
{
  if(channel!=1 && channel!=2)
    throw std::runtime_error("Invalid channel: "+std::to_string(channel));

  return std::stod(receive("V" + std::to_string(channel) + "O?").substr(0,5));
}

void SorensenPs::send(const std::string& cmd)
{
  m_com->write("++addr " + std::to_string(m_config["communication"]["gpib_addr"].get<uint32_t>()) + "\n\r");
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
  m_com->write(cmd+"\n\r");
  std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
}

std::string SorensenPs::receive(const std::string& cmd)
{
  m_com->write("++addr " + std::to_string(m_config["communication"]["gpib_addr"].get<uint32_t>()) + "\n\r");
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
  m_com->write(cmd+"\n\r");
  m_com->write("++read eoi\n\r");
  std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
  std::string buf;
  m_com->read(buf);
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Received: " << buf;
  return buf;
}
