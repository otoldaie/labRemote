#ifndef KEITHLEY22XX_H
#define KEITHLEY22XX_H

#include <chrono>
#include <memory>
#include <string>

#include "IPowerSupply.h"

#include "SerialCom.h"

/** \brief Keithley 22xx
 *
 * Implementation for the Keithley 2231A power supply.
 *
 * [Programming Manual](https://download.tek.com/manual/077100401_Referencemanual.pdf)
 *
 * Other Keithley 22xx power supplies might be supported too, but have
 * not been checked.
 *
 * WARNING: 2231 has not been tested either.
 */
class Keithley22XX : public IPowerSupply
{
public:
  Keithley22XX(const std::string& name);
  ~Keithley22XX() =default;

  /** \name Communication
   * @{
   */

  virtual void connect();

  virtual void checkCompatibilityList();

  /** @} */

  /** \name Power Supply Control
   * @{
   */

  virtual void reset();
  virtual void turnOn(unsigned channel);
  virtual void turnOff(unsigned channel);

  /** @} */
  
  /** \name Current Control and Measurement
   * @{
   */

  virtual void   setCurrentLevel(double cur, unsigned channel = 0);
  virtual double getCurrentLevel(unsigned channel = 0);
  virtual void   setCurrentProtect(double maxcur , unsigned channel = 0);
  virtual double getCurrentProtect(unsigned channel = 0);  
  virtual double measureCurrent(unsigned channel = 0);

  /** @} */

  /** \name Voltage Control and Measurement
   * @{
   */

  virtual void   setVoltageLevel(double volt, unsigned channel = 0);
  virtual double getVoltageLevel(unsigned channel = 0);
  virtual void   setVoltageProtect(double maxvolt , unsigned channel = 0 );
  virtual double getVoltageProtect(unsigned channel = 0);
  virtual double measureVoltage(unsigned channel = 0);

  /** @} */

public:
  void connect_usb(const std::string& dev);

private:
  std::unique_ptr<SerialCom> m_com=nullptr;

  void send(const std::string& cmd);
  std::string receive(const std::string& cmd);

  std::chrono::milliseconds m_wait{200};
};

#endif

