#ifndef RS_HMPXXXX_H
#define RS_HMPXXXX_H

#include <chrono>
#include <memory>
#include <string>

#include "IPowerSupply.h"

#include "SerialCom.h"

/** \brief ROHDE&SWARZ HMP4040
 * Implementation for the ROHDE&SWARZ HMP4040 power supply.
 *
 * [Programming Manual](https://cdn.rohde-schwarz.com/pws/dl_downloads/dl_common_library/dl_manuals/gb_1/h/hmp_serie/HMP_SCPI_ProgrammersManual_en_01.pdf)
 *
 * Others in the HMP series (HMP2020, HMP2030, HMP4030 and HMP4040) might
 * be also supported, but have not been tested.
 */
class RS_HMPXXXX : public IPowerSupply
{
public:
  RS_HMPXXXX(const std::string& name);
  ~RS_HMPXXXX() =default;

  /** \name Communication
   * @{
   */

  virtual void connect();

  virtual void checkCompatibilityList();

  /** @} */

  /** \name Power Supply Control
   * @{
   */

  virtual void reset();
  virtual void turnOn(unsigned channel);
  virtual void turnOff(unsigned channel);

  /** @} */
  
  /** \name Current Control and Measurement
   * @{
   */

  virtual void   setCurrentLevel(double cur, unsigned channel = 0);
  virtual double getCurrentLevel(unsigned channel = 0);
  virtual void   setCurrentProtect(double maxcur , unsigned channel = 0);
  virtual double getCurrentProtect(unsigned channel = 0);  
  virtual double measureCurrent(unsigned channel = 0);

  /** @} */

  /** \name Voltage Control and Measurement
   * @{
   */

  virtual void   setVoltageLevel(double volt, unsigned channel = 0);
  virtual double getVoltageLevel(unsigned channel = 0);
  virtual void   setVoltageProtect(double maxvolt , unsigned channel = 0 );
  virtual double getVoltageProtect(unsigned channel = 0);
  virtual double measureVoltage(unsigned channel = 0);

  /** @} */

public:
  void connect_usb(const std::string& dev);

private:
  std::unique_ptr<SerialCom> m_com=nullptr;

  void send(const std::string& cmd);
  std::string receive(const std::string& cmd);

  std::chrono::milliseconds m_wait{200};
};

#endif

