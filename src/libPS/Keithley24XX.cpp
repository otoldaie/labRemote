#include "Keithley24XX.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(Keithley24XX)

Keithley24XX::Keithley24XX(const std::string& name) :
IPowerSupply(name)
{ }

void Keithley24XX::connect()
{
  if (m_config["communication"]["protocol"] == "usb")
    {
      connect_usb(m_config["communication"]["port"]);

      std::string result = this->receive("*IDN?");
      if (result.empty())
	{
	  logger(logERROR) << "Failed communication with the PS";
	  throw std::runtime_error("Failed communication with the PS");
	}
    }
  else
    {
      logger(logERROR) << "Unknown communication protocol for hardware " << m_name << ": " << m_config["communication"]["protocol"];
      throw std::runtime_error("Unknown communication protocol for hardware " + m_name + ": " + m_config["communication"]["protocol"].get<std::string>());
    }
}

void Keithley24XX::connect_usb(const std::string& dev)
{
  m_com.reset(new SerialCom(dev));
  m_com->write("++auto 0\n\r");
}

void Keithley24XX::checkCompatibilityList()
{
  // Check if the PS belong to the list of the support PS models
  std::string idn=this->receive("*IDN?");

  //Add here other version of support Keithley 24xx power supplies
  if(idn.find("KEITHLEY INSTRUMENTS INC.,MODEL 2410") == std::string::npos)
    {
      logger(logERROR)<< "Unknown power supply: " << idn;
      throw std::runtime_error("Unknown power supply: " + idn);
    }
}

void Keithley24XX::reset()
{
  send("OUTPUT OFF");
  send("*RST");
  send(":TRIGGER:COUNT 1");
  send(":FORMAT:ELEMENTS TIME,VOLT,CURR");
  send(":SYST:BEEP:STAT OFF");//disable beep

  std::string result=receive("*IDN?");
  if(result.empty())
    throw std::runtime_error("No communication after reset.");
}

void Keithley24XX::turnOn(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  send("OUTPUT ON");
}

void Keithley24XX::turnOff(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");
  send("OUTPUT OFF");
}

void Keithley24XX::setCurrentLevel(double cur, unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");
  
  send(":SOURCE:FUNC CURR");
  send(":SOURCE:CURR " + to_string_with_precision(cur));  
}

double Keithley24XX::getCurrentLevel(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  return std::stod(receive(":SOURCE:CURR?"));
}

void Keithley24XX::setCurrentProtect(double maxcur, unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  send(":SENSE:CURR:PROTECTION " + std::to_string(maxcur));

}

double Keithley24XX::getCurrentProtect(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  return std::stod(receive(":SENSE:CURR:PROTECTION?"));

}

double Keithley24XX::measureCurrent(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  send(":FORMAT:ELEMENTS CURR");
  return std::stod(receive(":MEAS:CURR?"));
}

void Keithley24XX::setVoltageLevel(double volt, unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  send(":SOURCE:FUNC VOLT");
  send(":SOURCE:VOLT " + to_string_with_precision(volt));
}

double Keithley24XX::getVoltageLevel(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  return std::stod(receive(":SOURCE:VOLT?"));

}

void Keithley24XX::setVoltageProtect(double maxvolt, unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  send(":SENSE:VOLT:PROTECTION " + std::to_string(maxvolt));
}

double Keithley24XX::getVoltageProtect(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");


  return std::stod(receive(":SENSE:VOLT:PROTECTION?"));
}

double Keithley24XX::measureVoltage(unsigned channel)
{

  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  send(":FORMAT:ELEMENTS VOLT");
  return std::stod(receive(":MEAS:VOLT?"));

}

void Keithley24XX::send(const std::string& cmd)
{
  m_com->write("++addr " + std::to_string(m_config["communication"]["gpib_addr"].get<uint32_t>()) + "\n\r");
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
  m_com->write(cmd+"\n\r");
  std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
}

std::string Keithley24XX::receive(const std::string& cmd)
{
  m_com->write("++addr " + std::to_string(m_config["communication"]["gpib_addr"].get<uint32_t>()) + "\n\r");
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
  m_com->write(cmd+"\n\r");
  m_com->write("++read eoi\n\r");
  std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
  std::string buf;
  m_com->read(buf);
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Received: " << buf;
  return buf;
}
