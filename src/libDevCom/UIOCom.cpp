#include "UIOCom.h"

#include "ComIOException.h"
#include "NotSupportedException.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>

UIOCom::UIOCom(const std::string& device, uint32_t size)
  : m_size(size)
{
  m_fd = open(device.c_str(), O_RDWR);
  if(m_fd==-1)
    throw ComIOException("UIOCom cannot open "+device+": "+strerror(errno));

  m_ptr = (unsigned*)mmap(NULL, m_size, PROT_READ|PROT_WRITE, MAP_SHARED, m_fd, 0);
}

UIOCom::~UIOCom()
{
  munmap(m_ptr, m_size);
}

void UIOCom::write_reg32(uint32_t address, uint32_t data)
{ m_ptr[address] = data; }

void UIOCom::write_reg16(uint32_t address, uint16_t data)
{ m_ptr[address] = data; }

void UIOCom::write_reg8 (uint32_t address, uint8_t  data)
{ m_ptr[address] = data; }

void UIOCom::write_reg32(uint32_t data)
{ throw NotSupportedException("UIOCom does not support address-less writes."); }

void UIOCom::write_reg16(uint16_t data)
{ throw NotSupportedException("UIOCom does not support address-less writes."); }

void UIOCom::write_reg8 (uint8_t  data)
{ throw NotSupportedException("UIOCom does not support address-less writes."); }

void UIOCom::write_block(uint32_t address, const std::vector<uint8_t>& data)
{ throw NotSupportedException("UIOCom does not support block writes."); }

void UIOCom::write_block(const std::vector<uint8_t>& data)
{ throw NotSupportedException("UIOCom does not support block writes."); }

uint32_t UIOCom::read_reg32(uint32_t address)
{ return m_ptr[address]; }

uint16_t UIOCom::read_reg16(uint32_t address)
{ return m_ptr[address]; }

uint8_t  UIOCom::read_reg8 (uint32_t address)
{ return m_ptr[address]; }

uint32_t UIOCom::read_reg32()
{ throw NotSupportedException("UIOCom does not support address-less reads."); }

uint16_t UIOCom::read_reg16()
{ throw NotSupportedException("UIOCom does not support address-less reads."); }

uint8_t  UIOCom::read_reg8 ()
{ throw NotSupportedException("UIOCom does not support address-less reads."); }

void UIOCom::read_block(uint32_t address, std::vector<uint8_t>& data)
{ throw NotSupportedException("UIOCom does not support block reads."); }

void UIOCom::read_block(std::vector<uint8_t>& data)
{ throw NotSupportedException("UIOCom does not support block reads."); }


