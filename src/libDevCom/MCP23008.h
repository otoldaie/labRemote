#ifndef MCP23008_H
#define MCP23008_H

#include "IOExpander.h"
#include "I2CCom.h"

// Register map
#define MCP23008_IODIR   0x00
#define MCP23008_IPOL    0x01
#define MCP23008_GPINTEN 0x02
#define MCP23008_DEFVAL  0x03
#define MCP23008_INTCON  0x04
#define MCP23008_IOCON   0x05
#define MCP23008_GPPU    0x06
#define MCP23008_INTF    0x07
#define MCP23008_INTCAP  0x08
#define MCP23008_GPIO    0x09
#define MCP23008_OLAT    0x0A

class MCP23008 : public IOExpander
{
public:
  MCP23008(std::shared_ptr<I2CCom> com);
  virtual ~MCP23008() =default;

  virtual uint32_t getIO();
  virtual void setIO(uint32_t input);
  virtual void write(uint32_t value);
  virtual uint32_t read();

private:
  std::shared_ptr<I2CCom> m_com;
};

#endif // MCP23008_H
