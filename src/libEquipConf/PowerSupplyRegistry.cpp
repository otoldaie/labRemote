#include "PowerSupplyRegistry.h"

#include <iostream>

namespace EquipRegistry {

  typedef ClassRegistry<IPowerSupply, std::string> RegistryPS;

  static RegistryPS &registry()
  {
    static RegistryPS instance;
    return instance;
  }
  

  bool registerPowerSupply(const std::string& model, std::function<std::shared_ptr<IPowerSupply>(const std::string&)> f)
  {
    std::string model_lower = model;
    std::transform(model_lower.begin(), model_lower.end(), model_lower.begin(), ::tolower);
    return registry().registerClass(model_lower, f);
  }
  
  std::shared_ptr<IPowerSupply> createPowerSupply(const std::string& model, const std::string& name) {
    std::string model_lower = model;
    std::transform(model_lower.begin(), model_lower.end(), model_lower.begin(), ::tolower);
    auto result = registry().makeClass(model_lower, name);
    if(result == nullptr) {
      std::cout << "No Power Supply (IPowerSupply) of type " << model << ", matching the name '" << name << "' found\n";
    }
    return result;
  }
  
  std::vector<std::string> listPowerSupply() {
        return registry().listClasses();
  }

}

