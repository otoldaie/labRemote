option(BUILD_DOC "Build documentation" ON)

# Check if doxygen installed
find_package(Doxygen)
if (DOXYGEN_FOUND AND BUILD_DOC)

  # Doxygen configuration
  set(DOXYGEN_GENERATE_HTML YES)
  set(DOXYGEN_USE_MDFILE_AS_MAINPAGE ../README.md)
      
  # Create target
  doxygen_add_docs(
    doxygen
    ../README.md
    ../src/example/README.md
    ../src/tools/README.md
    ../src/libPS/README.md
    ../src
    COMMENT "Generate automatic documentation")
endif()
